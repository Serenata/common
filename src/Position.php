<?php

namespace Serenata\Common;

use JsonSerializable;
use OutOfBoundsException;

/**
 * Describes a position.
 *
 * This is a value object and immutable.
 *
 * @see https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#position
 */
final class Position implements JsonSerializable
{
    /**
     * The line, counting from zero.
     *
     * @var int
     */
    private $line;

    /**
     * The character index on the line, counting from zero.
     *
     * This is a character index, i.e. not byte offsets and "support" Unicode.
     *
     * @var int
     */
    private $character;

    /**
     * @param int $line
     * @param int $character
     */
    public function __construct(int $line, int $character)
    {
        $this->line = $line;
        $this->character = $character;
    }

    /**
     * Creates a new instance from the specified byte offset into the specified string and encoding.
     *
     * Note that this method will throw an (unchecked) OutOfBoundsException if the offset lies beyond the end of the
     * string. The end of the string itself is allowed, which can be useful for indicating the end of the string itself.
     *
     * @param int    $offset
     * @param string $string
     * @param string $encoding
     *
     * @return self
     */
    public static function createFromByteOffset(int $offset, string $string, string $encoding): self
    {
        if ($offset > strlen($string)) {
            throw new OutOfBoundsException("Offset {$offset} lies beyond length of string \"{$string}\"");
        }

        $part = substr($string, 0, $offset);

        $byteOffsetOfLineStart = $offset;

        while (--$byteOffsetOfLineStart >= 0) {
            if ($part[$byteOffsetOfLineStart] === "\n") {
                ++$byteOffsetOfLineStart;
                break;
            }
        }

        if ($byteOffsetOfLineStart < 0) {
            $byteOffsetOfLineStart = 0;
        }

        return new self(
            substr_count($part, "\n"),
            static::getCharacterOffsetFromByteOffset($offset, $string, $encoding) -
                static::getCharacterOffsetFromByteOffset($byteOffsetOfLineStart, $string, $encoding)
        );
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * @return int
     */
    public function getCharacter(): int
    {
        return $this->character;
    }

    /**
     * @param self $other
     *
     * @return bool
     */
    public function liesAfter(self $other): bool
    {
        if ($this->getLine() > $other->getLine()) {
            return true;
        } elseif ($this->getLine() === $other->getLine() && $this->getCharacter() > $other->getCharacter()) {
            return true;
        }

        return false;
    }

    /**
     * @param self $other
     *
     * @return bool
     */
    public function liesAfterOrOn(self $other): bool
    {
        if ($this->getLine() > $other->getLine()) {
            return true;
        } elseif ($this->getLine() === $other->getLine() && $this->getCharacter() >= $other->getCharacter()) {
            return true;
        }

        return false;
    }

    /**
     * @param self $other
     *
     * @return bool
     */
    public function liesBefore(self $other): bool
    {
        return !$this->liesAfterOrOn($other);
    }

    /**
     * @param self $other
     *
     * @return bool
     */
    public function liesBeforeOrOn(self $other): bool
    {
        return !$this->liesAfter($other);
    }

    /**
     * Calculates the byte offset this position has relative to the specified string in the specified encoding.
     *
     * @param string $string
     * @param string $encoding
     *
     * @return int
     */
    public function getAsByteOffsetInString(string $string, string $encoding): int
    {
        $i = 0;
        $currentLine = 0;
        $offsetOnLine = 0;
        $length = strlen($string);

        $lineStartIndex = null;

        while ($i < $length) {
            if ($string[$i] === "\n") {
                ++$currentLine;
                $offsetOnLine = 0;
                $lineStartIndex = $i + 1;
            }

            if ($currentLine === $this->getLine()) {
                $characterOffsetOnLine =
                    $this->getCharacterOffsetFromByteOffset(
                        $offsetOnLine,
                        substr($string, $lineStartIndex ?? 0),
                        $encoding
                    );

                if ($characterOffsetOnLine === $this->getCharacter()) {
                    if ($lineStartIndex !== null) {
                        return $i  + 1 ;
                    } else {
                        return $i;
                    }
                }
            }

            ++$i;
            ++$offsetOnLine;
        }

        $lastCharacter = $this->getCharacterOffsetFromByteOffset(
            $length,
            substr($string, 0),
            $encoding
        );

        if ($this->getLine() === 0 && $this->getCharacter() === $lastCharacter) {
            return $length;
        }

        throw new \OutOfBoundsException(
            "Line {$this->getLine()} and character {$this->getCharacter()} are not in range of string"
        );
    }

    /**
     * Retrieves the character offset from the specified byte offset in the specified string. The result will always be
     * smaller than or equal to the passed in value, depending on the amount of multi-byte characters encountered.
     *
     * @param int    $byteOffset
     * @param string $string
     * @param string $encoding
     *
     * @return int
     */
    private static function getCharacterOffsetFromByteOffset(int $byteOffset, string $string, string $encoding): int
    {
        return mb_strlen(mb_strcut($string, 0, $byteOffset, $encoding), $encoding);
    }

    /**
     * {@inheritDoc}
     *
     * @return array{line: int, character: int}
     */
    public function jsonSerialize(): array
    {
        return [
            'line'      => $this->getLine(),
            'character' => $this->getCharacter()
        ];
    }
}
