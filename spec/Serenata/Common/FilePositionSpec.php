<?php

namespace spec\Serenata\Common;

use Serenata\Common\Position;

use PhpSpec\ObjectBehavior;

class FilePositionSpec extends ObjectBehavior
{
    /**
     * @return void
     */
    public function it_returns_configured_file(): void
    {
        $this->beConstructedWith('Test', new Position(0, 0));

        $this->getFile()->shouldBe('Test');
    }

    /**
     * @return void
     */
    public function it_returns_configured_position(): void
    {
        $position = new Position(0, 0);

        $this->beConstructedWith('Test', $position);

        $this->getPosition()->shouldBe($position);
    }
}
