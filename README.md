# Serenata - Common
Contains common code that is shared by various parts of [Serenata](https://gitlab.com/Serenata/Serenata).

This project falls under the [Serenata](https://gitlab.com/Serenata) umbrella.

## Installation
```sh
composer require "serenata/common"
```

![GPLv3 Logo](https://gitlab.com/Serenata/Serenata/raw/793c93b0f69a5f4ba183f1dfff79f0c68d9bd010/resources/images/gpl_v3.png)
